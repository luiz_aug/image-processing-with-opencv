import unittest
from OpticalMarkRecognition import getAnswers

class TestGetAnswers(unittest.TestCase):
    def test_testFilled(self):
        self.assertEqual(getAnswers("Files/Tests/testFilled.png"), ([0,1,2,3,4,0,1,2,3,4,0,1,2,3,4,0,1,2,3,4,0,1,2,3,4,4,3,2,1,0,4,3,2,1,0,4,3,2,1,0,4,3,2,1,0,4,3,2,1,0],0))
    def test_1_25_Correct_0_Pen(self):
        self.assertEqual(getAnswers("Files/Tests/1_25_Correct_0_Pen.jpg"), ([0,1,2,3,4,3,2,1,0,1,2,3,4,3,2,1,0,1,2,3,4,3,2,1,0,0,1,2,3,4,3,2,1,0,1,2,3,4,3,2,1,0,1,2,3,4,3,2,1,0],0))
    def test_Correct_0_Pencil(self):    
        self.assertEqual(getAnswers("Files/Tests/Correct_0_Pencil.jpg"), ([0,1,2,3,4,3,2,1,0,1,2,3,4,3,2,1,0,1,2,3,4,3,2,1,0,4,3,2,1,0,1,2,3,4,3,2,1,0,1,2,3,4,3,2,1,0,1,2,3,4],0))
    def test_Duplicates_0_Pen(self):
        self.assertEqual(getAnswers("Files/Tests/Duplicates_0_Pen.jpg"), ([-1,-1,2,-1,-1,-1,2,-1,-1,-1,2,-1,-1,-1,2,-1,-1,-1,2,-1,-1,-1,2,-1,-1,-1,-1,-1,-1,-1,-1,-1,-1,-1,-1,-1,-1,-1,-1,-1,-1,-1,-1,-1,-1,-1,-1,-1,-1,-1],0))
    def test_Irregular_Correct_0_Pencil(self):
        self.assertEqual(getAnswers("Files/Tests/Irregular_Correct_0_Pencil.jpg"), ([-1,-1,-1,-1,-1,-1,-1,-1,-1,-1,-1,-1,-1,-1,-1,-1,-1,-1,-1,-1,-1,-1,-1,-1,-1,-1,-1,-1,-1,-1,-1,-1,-1,-1,-1,-1,-1,-1,-1,-1,-1,-1,-1,-1,-1,-1,-1,-1,-1,-1],0))
    def test_Correct_1_Pencil(self):
        self.assertEqual(getAnswers("Files/Tests/Correct_1_Pencil.jpg"), ([0,4,1,3,2,1,3,0,4,1,3,2,1,3,0,4,1,3,2,1,3,0,4,1,3,0,4,1,3,2,1,3,0,4,1,3,2,1,3,0,4,1,3,2,1,3,0,4,1,3],0))
    #def test_Irregularities_2_Pencil(self):
    #     self.assertEqual(getAnswers("Files/Tests/Irregularities_2_Pencil.jpg"), ([0,-1,1,-1,2,-1,3,-1,4,-1,3,-1,1,-1,0,-1,1,-1,2,-1,3,-1,4,-1,3,0,4,1,3,2,1,3,-1,-1,-1,-1,2,-1,-1,0,4,1,3,2,1,3,0,4,1,-1],0))
