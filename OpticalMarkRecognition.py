# Python and OpenCV OMR Project
# Objective:    Create a OMR to automatically grade tests based on a test-template.
#               It should support up to 4 different test models, each test made of 50 questions
# Authors:      Luiz A. Bernardi & Marcos Doff
# Date:         Nov 2020
# NOTE:         Input image should be corrected in terms of perspective through a previous image processing.

# LIBRARIES
from cv2 import cv2
import numpy as np
import tools
import constants
import warnings


def getAnswers(fileName="Files/Tests/Template_No_TestType.jpg"):
    # USER CONFIGURATIONS
    aspectRatio = 1.35  # Known data
    scaleFactor = 2

    # TODO:remove the next line
    template = 0

    # LOAD IMAGE
    originalImage = cv2.imread(fileName)  # Default BRG Color
    originalShape = originalImage.shape  # [height , witdh , channels]

    # RESIZE IMAGE - PERFORMANCE ENHANCE
    scaleWidth = round(originalShape[1] / scaleFactor)
    scaleHeight = round(scaleWidth * aspectRatio)
    scaleImage = cv2.resize(originalImage, (scaleWidth, scaleHeight))

    # COLOR CONVERTION
    grayImage = cv2.cvtColor(scaleImage, cv2.COLOR_BGR2GRAY)

    # NOISE FILTERING
    blurImage = cv2.GaussianBlur(grayImage, (5, 5), 1)

    # BORDER DETECTION
    borderImage = cv2.Canny(blurImage, 100, 200)

    # MORPHOLOGIC PROCESSING
    structElement = cv2.getStructuringElement(cv2.MORPH_ELLIPSE, (2, 2))
    borderImage = cv2.morphologyEx(borderImage, cv2.MORPH_DILATE, structElement)

    # COUNTOURS
    countoursImage = scaleImage.copy()
    countours, hierarchy = cv2.findContours(
        borderImage, cv2.RETR_EXTERNAL, cv2.CHAIN_APPROX_SIMPLE
    )
    cv2.drawContours(countoursImage, countours, -1, (0, 0, 255), 5)

    # EXTRACT TEST TYPE
    testTypeContour, isTemplate = tools.extractTestTypeContour(countours)

    testVariantWindow = tools.getCornerPoints(testTypeContour[len(testTypeContour) - 1])

    testVariantImage = blurImage[
        testVariantWindow[0, 0, 1] : testVariantWindow[1, 0, 1],
        testVariantWindow[0, 0, 0] : testVariantWindow[2, 0, 0],
    ]

    testVariant = tools.extractTestVariant(testVariantImage)

    # PROCESS ANSWERS
    ans1_25, ans26_50 = tools.extractAnswersContours(countours)

    ans1_25Window = tools.getCornerPoints(ans1_25)
    ans26_50Window = tools.getCornerPoints(ans26_50)

    ans1_25Image = blurImage[
        ans1_25Window[0, 0, 1] : ans1_25Window[1, 0, 1],
        ans1_25Window[0, 0, 0] : ans1_25Window[2, 0, 0],
    ]

    ans26_50Image = blurImage[
        ans26_50Window[0, 0, 1] : ans26_50Window[1, 0, 1],
        ans26_50Window[0, 0, 0] : ans26_50Window[2, 0, 0],
    ]
    answers = tools.extractAnswers(ans1_25Image, 25) + tools.extractAnswers(
        ans26_50Image, 25
    )

    # NOTE: DEBUG
    # cv2.imshow("Output1", countoursImage)
    # cv2.imwrite("Contorus.jpg", countoursImage)
    # cv2.imshow("Answer 1 - 25", ans1_25Image)
    # cv2.imwrite("Answer 1 - 25.jpg", ans1_25Image)

    # print(answers)
    # cv2.imshow("Answer 26 - 50", ans26_50Image)
    # cv2.waitKey(0)

    return answers, testVariant, isTemplate


def main():
    while True:
        
        fileName = input("Which file do you wish to analyse?")

        answers, testVariant, isTemplate = getAnswers(fileName)

        if len(answers) != 50 or testVariant < 0:
            print(
                "Error: Total answers read: "
                + str(len(answers))
                + " Test Variant:"
                + str(testVariant)
            )
            return
        else:
            templateFileName = (
                "./Files/Templates/Variant "
                + str(testVariant)
                + "/Template"
                + str(testVariant)
                + ".py"
            )

        if isTemplate:
            tools.saveTemplate(answers, testVariant, templateFileName)
        else:
            correctAnswers = tools.getTemplate(templateFileName)

            studentGrade = tools.grade(answers, correctAnswers)
            #print("Student Grade: " + str(studentGrade))
            print("Student Grade: {:.1f}".format(studentGrade))

        if input("Do you wish to analyse another file?(y/n) ") == "y":
            pass
        else:
            break


if __name__ == "__main__":
    warnings.filterwarnings("ignore")
    main()
