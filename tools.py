import numpy as np
import matplotlib.pyplot as plt
from cv2 import cv2
from math import trunc
import constants


def extractAnswers(answersCrop, numQuestions):
    # This function will receive one of the crops that contain only the answers
    answerArray = []
    shapeAnswers = answersCrop.shape

    # Add black border on left and right margins
    answersCrop = answersCrop[5 : shapeAnswers[0] - 5, 0 : shapeAnswers[1]]
    answersCrop = cv2.copyMakeBorder(answersCrop, 5, 5, 0, 0, cv2.BORDER_CONSTANT)

    # NOTE: DEBUG
    # cv2.imwrite("answersCrop.png", answersCrop)

    rowSums = np.zeros(shapeAnswers[0])

    for i in range(shapeAnswers[0]):
        for j in range(shapeAnswers[1]):
            rowSums[i] = rowSums[i] + answersCrop[i, j]

    divisions = []  # will contain the rows with horizontal lines
    aux = 0

    for i in range(1, len(rowSums) - 1):
        if rowSums[i] <= 15000:
            if i == aux + 1:
                aux = i
            else:
                divisions.append(aux + 1)
                divisions.append(i - 1)
                aux = i

    for i in range(numQuestions):
        if i >= numQuestions:
            break

        singleAnswerCrop = answersCrop[
            divisions[2 * i] + 2 : divisions[2 * i + 1] - 2, 5 : shapeAnswers[1] - 5
        ]
        singleAnswerCrop = cv2.normalize(
            singleAnswerCrop,
            None,
            alpha=0,
            beta=1.2,
            norm_type=cv2.NORM_MINMAX,
            dtype=cv2.CV_32F,
        )

        # NOTE: DEBUG
        # cv2.imshow("crop",singleAnswerCrop)
        # cv2.waitKey(0)

        # Each question will be seaparated into 5 equal parts and the one with the lowest average is the
        # one to be considered as marked
        answer = -1  # a-e -> 0-4, -1 means that more than one answer or no answer
        shape = singleAnswerCrop.shape
        columnSum = np.zeros(shape[1])
        columnBool = np.zeros(shape[1])

        for k in range(shape[1]):
            for l in range(shape[0]):
                columnSum[k] = columnSum[k] + singleAnswerCrop[l, k]

        columnSum = columnSum.max() - columnSum
        columnSum = columnSum / columnSum.max()

        for k in range(shape[1]):
            if columnSum[k] >= 0.8:
                columnBool[k] = 1

        # NOTE: DEBUG
        # columnBool = np.gradient(columnBool)
        # plt.plot(columnBool)
        # plt.plot(columnSum)
        # plt.show()

        step = trunc(shape[1] / 5)
        marks = np.zeros(5)
        for index in range(5):
            if sum(columnBool[index * step : (index + 1) * step]) > 0:
                marks[index] = 1

        if sum(marks) <= 0 or sum(marks) > 1:
            answer = -1
        else:
            answer = np.argmax(marks)

        answerArray.append(answer)

    return answerArray


def extractAnswersContours(contours):
    # Set inicial conditions to find the 2 biggest areas
    anscontourIndex1 = -1
    anscontourIndex2 = -1
    areamax = 0
    areasecondmax = 0
    i = 0

    # Look countours for the 2 biggest areas
    for cnt in contours:
        if cv2.contourArea(cnt) >= areamax:
            areasecondmax = areamax
            areamax = cv2.contourArea(cnt)
            anscontourIndex2 = anscontourIndex1
            anscontourIndex1 = i
        elif cv2.contourArea(cnt) > areasecondmax:
            areasecondmax = cv2.contourArea(cnt)
            anscontourIndex2 = i
        i += 1

    # Determine answers blocks order - The first block of answers have smaller X-axis components
    if max(contours[anscontourIndex1][:, 0, 0]) > max(
        contours[anscontourIndex2][:, 0, 0]
    ):
        anscontour1_25 = contours[anscontourIndex2][:, 0, :]
        anscontour26_50 = contours[anscontourIndex1][:, 0, :]
    else:
        anscontour1_25 = contours[anscontourIndex1][:, 0, :]
        anscontour26_50 = contours[anscontourIndex2][:, 0, :]
    return anscontour1_25, anscontour26_50


def extractTestTypeContour(contours):
    # Initial definitions
    yLimit = 100  # Value estimated from the template
    xMax = []
    candidateContours = []
    testTypeContours = []

    # Look for contours above yLimit and register their xMax
    i = 0
    for cnt in contours:
        if contours[i][:, :, 1].max() < yLimit:
            candidateContours.append(cnt)
            xMax.append(cnt[:, 0].max())
        i += 1

    # Order all xMax registered values
    xMaxOrdered = xMax.copy()
    xMaxOrdered.sort()

    # It is known that only the second and (if present) the fouth fields matter,
    # then the xMax value is used to delete other fields.
    testTypeContours.append(candidateContours[xMax.index(xMaxOrdered[1])])
    if len(candidateContours) > 2:
        testTypeContours.append(
            candidateContours[xMax.index(xMaxOrdered[len(xMaxOrdered) - 1])]
        )
        isTemplate = True
    else:
        isTemplate = False

    return testTypeContours, isTemplate


def extractTestVariant(testVariantCrop):
    # cv2.imwrite("TestVariant.jpeg", testVariantCrop)

    # Test variant will be seaparated into 5 equal parts and the one with the lowest average is the
    # one to be considered as marked
    candidateMean = []
    shape = testVariantCrop.shape
    offset = 6
    testVariantCrop = testVariantCrop[
        offset : shape[0] - offset, offset : shape[1] - offset
    ]
    shape = testVariantCrop.shape
    xStep = trunc((shape[1]) / 4)

    for j in range(4):
        candidateMean.append(
            testVariantCrop[0 : shape[0], j * xStep : (j + 1) * xStep].mean()
        )
        # cv2.imshow(
        #    "TestVariant",
        #    testVariantCrop[0 : shape[0], j * xStep : (j + 1) * xStep])
        # cv2.waitKey()
    arrayCandidateMean = np.array(candidateMean)
    candidate = candidateMean.index(min(candidateMean))

    # Test for exceptional cases: No test type marked / multiple test type marked
    # FIXME: Test other threshold values and techniques (i.e:amplitude)
    # NOTE: It is possible to break this block and individuallly identify multiple
    #       marked questions as well as no marked questions
    if (
        np.std(candidateMean) < 6
        or len(
            arrayCandidateMean[
                np.where(arrayCandidateMean <= np.mean(arrayCandidateMean))
            ]
        )
        > 1
    ):
        return -1
    else:
        return candidate


def getCornerPoints(contour):
    perimeter = cv2.arcLength(contour, True)
    approx = cv2.approxPolyDP(contour, 0.02 * perimeter, True)
    return approx


def grade(answers, template):
    if not (len(answers) == len(template)):  # simple exception treatment
        return -1

    gradeArray = np.equal(answers, template)

    weights = constants.questionWeights
    weights = (1/sum(weights))*weights

    grade = np.multiply(gradeArray,weights)

    return 10*sum(np.multiply(gradeArray,weights))


def saveTemplate(answers, testVariant, templateFileName):
    if len(np.array(answers)[np.where(np.array(answers) == -1)]) > 0:
        print("Error: Template with not recognized answers\n")
    else:
        try:
            with open(templateFileName, "w") as fileHandle:
                for answer in answers:
                    fileHandle.write("%s\n" % answer)
                fileHandle.close()
            print(
                "Success: Template "
                + str(testVariant)
                + " saved on "
                + str(templateFileName)
            )
        except:
            print(
                "Error: Template "
                + str(testVariant)
                + " could not be saved on "
                + str(templateFileName)
            )
    return


def getTemplate(templateFileName):
    correctAnswers = []
    try:
        with open(templateFileName, "r") as filehandle:
            for line in filehandle:
                # remove '\n' char
                currentAnswer = line[:-1]
                # add item
                correctAnswers.append(int(currentAnswer))
            filehandle.close()
    except:
        print("Error: Template file could not be read from" + str(templateFileName))
    return correctAnswers