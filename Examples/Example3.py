# Python and OpenCV Example 3
# Objective:   Explore simple image manipulation
# Author:      Luiz A. Bernardi
# Date:        Oct 2020
# NOTE 1: Check OpenCV functtions documentation at https://docs.opencv.org
# NOTE 2: This code is neither NOT OPTIMEZED or GENERIC at any level. 

from cv2 import cv2
from matplotlib import pyplot as plt
import numpy as np

# LOAD IMAGE
colorImage = cv2.imread("Files/Raven.tif")


# CONVERT COLORSPACES
colorImage = cv2.cvtColor(colorImage, cv2.COLOR_BGR2RGB) # OpenCV uses BGR colorspace as default
grayImage = cv2.cvtColor(colorImage, cv2.COLOR_RGB2GRAY) 


# FILTERS
blurImage = cv2.GaussianBlur(grayImage,(5,5),1) # Default gaussian filter: 5x5 stdev: 1 
biFilteredImage = cv2.bilateralFilter(grayImage,None,125,200,200,cv2.BORDER_DEFAULT)


# BORDER EXTRATION
borderImage = cv2.Canny(biFilteredImage,45,210,None,5,True) # Histeresis from 50 to 210, Sobel 5x5 and
                                                            # sqrt method for X and Y gradients combination


# THRESHOLDING
ret,bwBorder = cv2.threshold(borderImage,127,255,cv2.THRESH_BINARY+cv2.THRESH_OTSU)
bwImage = cv2.adaptiveThreshold(blurImage,255,cv2.ADAPTIVE_THRESH_GAUSSIAN_C, \
                                cv2.THRESH_BINARY_INV,125,0) # Block size (and Gaussian Kernel): 25x25
                                                             # MaxVal = 255
                                                             # C = 3 is used to lower threshold calculated from the gaussian kernel.
# IMAGE BLENDING
bwImage2 = bwImage - bwBorder             


# MORPHOLOGIC OPERATIONS
structElement = cv2.getStructuringElement(cv2.MORPH_ELLIPSE,(6,6))
dilatedBorders = cv2.morphologyEx(bwBorder,cv2.MORPH_DILATE,structElement)

structElement = cv2.getStructuringElement(cv2.MORPH_ELLIPSE,(5,5))
closeImage = cv2.morphologyEx(dilatedBorders,cv2.MORPH_CLOSE,structElement)

structElement = cv2.getStructuringElement(cv2.MORPH_ELLIPSE,(15,15))
openImage = cv2.morphologyEx(closeImage,cv2.MORPH_OPEN,structElement)
structElement = cv2.getStructuringElement(cv2.MORPH_ELLIPSE,(40,40))
openImage = cv2.morphologyEx(closeImage,cv2.MORPH_OPEN,structElement) + bwBorder


# IMAGE MASKING
maskImage = np.zeros_like(colorImage)
maskImage[:,:,0] = openImage - bwBorder
maskImage[:,:,1] = openImage - bwBorder
maskImage[:,:,2] = openImage - bwBorder
maskedImage = cv2.bitwise_and(colorImage,maskImage,None,None)

# DEBUG INFO
print("********** DEBUG **********")
print("Bw Image:", '\t', "Size:",len(bwImage) , "," , len(bwImage[0]), 
      "Min:", bwImage.min(), "Max:", bwImage.max(), "Mean:", bwImage.mean() ,type(bwImage))
print("Bw Border;", '\t', "Size:",len(bwBorder) , "," , len(bwBorder[0]),
      "Min:", bwBorder.min(), "Max:", bwBorder.max(), "Mean:", bwBorder.mean() ,type(bwBorder))
print("Mask Image Shape:",maskImage.shape)
print("Mask Image Data Type",maskImage.dtype)

# BUILD MULTIPLOT FIGURE
plt.figure()
plt.subplots_adjust(top = 0.95, bottom = 0, right = 1, left = 0, 
                    hspace = 0.1, wspace = 0.025)
# SHOW RESULTS
plt.subplot(2, 4, 1), 
plt.imshow(colorImage), 
plt.axis("off"), 
plt.title("Original Image - RGB")

plt.subplot(2, 4, 2), 
plt.imshow(grayImage,"gray"), 
plt.axis("off"), 
plt.title("Color Changed Image - Gray")

plt.subplot(2, 4, 3), 
plt.imshow(blurImage, "gray"), 
plt.axis("off"), 
plt.title("Blured Image - Gray")

plt.subplot(2, 4, 4), 
plt.imshow(bwBorder, "gray"), 
plt.axis("off"), 
plt.title("Borders - Gray")

plt.subplot(2, 4, 5), 
plt.imshow(bwImage2, "gray"), 
plt.axis("off"), 
plt.title("Combined Borders + BW - BW")

plt.subplot(2, 4, 6), 
plt.imshow(closeImage, "gray"), 
plt.axis("off"), 
plt.title("Close Operation - BW")

plt.subplot(2, 4, 7), 
plt.imshow(openImage, "gray"), 
plt.axis("off"), 
plt.title("Open Operation - BW")

plt.subplot(2, 4, 8), 
plt.imshow(maskedImage),
plt.axis("off"), 
plt.title("Masked Image - RGB")


# SAVE FIGURE TO A FILE
plt.savefig("Outputs/ravenPlot",dpi=300)    # This has to be done before showing the image, 
                                            # otherwise a new (blank) image is created


# SHOW IMAGE
plt.show() # To show figure