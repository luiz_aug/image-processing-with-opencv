# Python and OpenCV Example 5
# Objective:   Explore drawing functions built in OpenCV
# Author:      Luiz A. Bernardi
# Date:        Oct 2020
# NOTE 1: Check OpenCV functtions documentation at https://docs.opencv.org
# NOTE 2: Notice the offset when drawing lines on image's borders

from cv2 import cv2
import numpy as np

# CREATE BLACK IMAGE
grayScaleCanvas = np.zeros((480,600),np.uint8) # Height,Width - 0..255
colorCanvas = np.zeros((480,600,3),np.uint8) # Height,Width,RGB - 0..255

# DRAW LINES - TRIANGLE
cv2.line(grayScaleCanvas,
        (0,grayScaleCanvas.shape[0]),                           # Tuple : (width, height) .. shape: [height, width, channels]
        (round(grayScaleCanvas.shape[1]/2),0),                  # Tuple : (width, height) .. shape: [height, width, channels]
        255,2)                                                  # Color: 255 .. Thickness: 2
cv2.line(grayScaleCanvas,
        (round(grayScaleCanvas.shape[1]/2),0),                  # Tuple : (width, height) .. shape: [height, width, channels]
        (grayScaleCanvas.shape[1],grayScaleCanvas.shape[0]),    # Tuple : (width, height) .. shape: [height, width, channels]
        255,2)                                                  # Color: 255 .. Thickness: 2
cv2.line(grayScaleCanvas,
        (grayScaleCanvas.shape[1],grayScaleCanvas.shape[0]),    # Tuple : (width, height) .. shape: [height, width, channels]
        (0,grayScaleCanvas.shape[0]),                           # Tuple : (width, height) .. shape: [height, width, channels]
        255,2)                                                  # Color: 255 .. Thickness: 2

cv2.line(colorCanvas,
        (0,colorCanvas.shape[0]),                               # Tuple : (width, height) .. shape: [height, width, channels]
        (round(colorCanvas.shape[1]/2),0),                      # Tuple : (width, height) .. shape: [height, width, channels]
        (255,0,0),                                              # Color: (255,0,0) - BGR
        2)                                                      # Thickness: 2
cv2.line(colorCanvas,
        (round(colorCanvas.shape[1]/2),0),                      # Tuple : (width, height) .. shape: [height, width, channels]
        (colorCanvas.shape[1],colorCanvas.shape[0]),            # Tuple : (width, height) .. shape: [height, width, channels]
        (0,255,0),                                              # Color: (0,255,0) - BGR
        2)                                                      # Thickness: 2
cv2.line(colorCanvas,
        (colorCanvas.shape[1],colorCanvas.shape[0]),            # Tuple : (width, height) .. shape: [height, width, channels]
        (0,colorCanvas.shape[0]),                               # Tuple : (width, height) .. shape: [height, width, channels]
        (0,0,255),                                              # Color: (0,0,255) - BGR
        2)                                                      # Thickness: 2

# DRAW RECTANGLE
imageCenter = (round(colorCanvas.shape[0]/2),round(colorCanvas.shape[1]/2))
cv2.rectangle(grayScaleCanvas,(imageCenter[1]-100,imageCenter[0]-80),(imageCenter[1]+100,imageCenter[0]+80),255,2)
cv2.rectangle(colorCanvas,(imageCenter[1]-100,imageCenter[0]-80),(imageCenter[1]+100,imageCenter[0]+80),(200,127,200),2)

# DRAW CIRCLE 
cv2.circle(grayScaleCanvas,(imageCenter[1],imageCenter[0]),imageCenter[0],255,2)
cv2.circle(colorCanvas,(imageCenter[1],imageCenter[0]),imageCenter[0],(123,231,255),2)

# TEXT ON IMAGE
cv2.putText(grayScaleCanvas,"ABCDE",(imageCenter[1]-50,imageCenter[0]+75),cv2.FONT_HERSHEY_PLAIN,2,255,2)
cv2.putText(colorCanvas,"ABCDE",(imageCenter[1]-50,imageCenter[0]+75),cv2.FONT_HERSHEY_PLAIN,2,(50,50,255),2)

# SHOW RESULTS
print("GrayScale Canvas Size: ", grayScaleCanvas.shape)
print("Color Canvas Size: ", grayScaleCanvas.shape)

cv2.imshow("GrayScale Canvas", grayScaleCanvas)
cv2.imshow("Color Canvas", colorCanvas)

cv2.waitKey(10000)