# Python and OpenCV Example 4
# Objective:   Explore cropping and resize functionalities
# Author:      Luiz A. Bernardi
# Date:        Oct 2020
# NOTE 1: Check OpenCV functtions documentation at https://docs.opencv.org

from cv2 import cv2

# LOAD IMAGE
originalImage = cv2.imread("Files/Lenna256g.png")

# RESIZING -> 1/2 SCALE
scaleHeight = round(len(originalImage) / 2)
scaleWidth = round(len(originalImage[1]) / 2)
smallerImage = cv2.resize(originalImage, (scaleHeight, scaleWidth))

# CROPPING
croppedImage = originalImage[50 : len(originalImage), 50 : 200] # cv2Matrix[heightAxis, widthAxis]

# SHOW RESULTS
cv2.imshow("Original Image", originalImage)
cv2.imshow("Resized Image", smallerImage)
cv2.imshow("Cropped Image", croppedImage)

cv2.waitKey(5000)
