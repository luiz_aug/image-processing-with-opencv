# Python and OpenCV Example 1
# Objective: Read all images from a folder and show them on the screen
# Author: Luiz A. Bernardi
# Date: Oct 2020

from cv2 import cv2
import os
import numpy as np

# READING IMAGES
imgList = list()
path, dirs, files = next(os.walk("Files"))
for image in files:
    fileName = path + "/" + image
    img = cv2.imread(fileName)
    imgList.append(img)

# SHOWING IMAGES
for imgIndex in range(len(imgList)):
    cv2.imshow("Output", imgList[imgIndex])
    cv2.waitKey(1000)
