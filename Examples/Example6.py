# Python and OpenCV Example 5
# Objective:   Explore OpenCV perspective correction
# Author:      Luiz A. Bernardi
# Date:        Oct 2020
# NOTE 1: Check OpenCV functtions documentation at https://docs.opencv.org

from cv2 import cv2
from matplotlib import pyplot as plt
import numpy as np

# LOAD IMAGE
originalImage = cv2.imread("Files/playCard.png")

# CONVERT COLORSPACES
originalImage = cv2.cvtColor(originalImage, cv2.COLOR_BGR2RGB) # OpenCV uses BGR colorspace as default
grayImage = cv2.cvtColor(originalImage, cv2.COLOR_RGB2GRAY) 

# FILTER IMAGE
biFilteredImage = cv2.bilateralFilter(grayImage,None,50,75,75,cv2.BORDER_DEFAULT) # 75 75

# THRESHOLD
ret, bwImage = cv2.threshold(biFilteredImage,127,255,cv2.THRESH_OTSU+cv2.THRESH_BINARY)

# PERSPECTIVE TRANSFORMATION
controlPoints = np.float32([[465,475],[975,235],[1330,1030],[840,1270]]) # Width, Height
targetPoints = np.float32([[0,0],[570,0],[570,870],[0,870]]) # Width, Height
transfMatrix = cv2.getPerspectiveTransform(controlPoints, targetPoints)
perspectiveImage = cv2.warpPerspective(bwImage,transfMatrix,(570,870))

# BUILD MULTIPLOT FIGURE
plt.figure()
plt.subplots_adjust(top = 0.95, bottom = 0, right = 1, left = 0, 
                    hspace = 0.1, wspace = 0.025)
# SHOW RESULTS
plt.subplot(1, 5, 1), 
plt.imshow(originalImage), 
plt.axis("off"), 
plt.title("Original Image - RGB")

plt.subplot(1, 5, 2), 
plt.imshow(grayImage,"gray"), 
plt.axis("off"), 
plt.title("Color Changed Image - Gray")

plt.subplot(1, 5, 3), 
plt.imshow(biFilteredImage, "gray"), 
plt.axis("off"), 
plt.title("Filtered Image - Gray")

plt.subplot(1, 5, 4), 
plt.imshow(bwImage, "gray"), 
plt.axis("off"), 
plt.title("BW Otsu - BW")

plt.subplot(1, 5, 5), 
plt.imshow(perspectiveImage, "gray"), 
plt.axis("off"), 
plt.title("Perspective - BW")

# SAVE FIGURE TO A FILE
plt.savefig("Outputs/cardPlot",dpi=300)    # This has to be done before showing the image, 
                                            # otherwise a new (blank) image is created

plt.show()
