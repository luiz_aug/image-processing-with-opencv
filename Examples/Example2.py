# Python and OpenCV Example 2
# Objective:   Capture an image from the webcam
# Author:      Luiz A. Bernardi
# Date:        Oct 2020
# NOTE:        If your IDE has no permission to access camera in mac osX,
#              call it via terminal and make sure that terminal has the
#              permissions to access the camera.
#              More info here: https://streamable.com/zsa8ro

from cv2 import cv2

# WINDOW AND CAMERA SETTINGS
cv2.namedWindow("Output")
capture = cv2.VideoCapture(0)  # ID 0 selects the built-in camera
capture.set(3, 860)  # ID 3 = image width
capture.set(4, 360)  # ID4 = image height

while True:
    rval, frame = capture.read()  # single frame read

    if frame is not None:
        cv2.imshow("Output", frame)

    if cv2.waitKey(1) & 0xFF == ord("q"):  # 'q' will stop execution
        break
